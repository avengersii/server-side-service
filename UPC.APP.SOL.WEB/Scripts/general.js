﻿function ValidateRangoFechas(id1, id2, message) {

    var datetime1 = document.getElementById(id1).value.split('/');
    var datetime2 = document.getElementById(id2).value.split('/');

    var date1 = new Date(datetime1[2], datetime1[1], datetime1[0]);
    var date2 = new Date(datetime2[2], datetime2[1], datetime2[0]);

    if (date1 > date2) {
        debugger;
        MessageValidacion(message);
        return true;
    }
    return false;
}


function MessageValidacion(message) {
    $("#btnmessageAlert").click();
    $("#messageAlert").html(message);
}


function isEmptyControl(id, message) {
    var ctl = document.getElementById(id);
    var blnEmpty = false;

    if (ctl == null) {
        alert('El control <' + id + '> no existe');
        return true;
    }
    switch (ctl.type) {
        case 'text':
        case 'textarea':
        case 'hidden':
        case 'select-one':
            if (ctl.value.trim() == '') {
                blnEmpty = true;
                break;
            }
            else {
                switch (ctl.className) {
                    case 'esdNumericTextBox':
                    case 'esdNumericTextBoxDisabled':
                        var dec = parseFloat(ctl.value);
                        if (dec == null) {
                            blnEmpty = true;
                            break;
                        }
                        else {
                            if (dec == 0) {
                                blnEmpty = true;
                                break;
                            }
                            else {
                                blnEmpty = false;
                                break;
                            }
                        }
                        break;
                    default:
                        blnEmpty = false;
                }
            }
            break;
    }

    if (blnEmpty && message != '') {
        alert(message);
        ctl.focus();
    }

    return blnEmpty;
}

function MostrarAlerta(msg) {
    $("#msgAlerta").html(msg).css("display", "block");
}

function getValueControl(idControl) {
    
    return document.getElementById(idControl).value;
}

function getValueChecked(idControl) {

    return document.getElementById(idControl).checked;
}

function convertParametro(value) {
    if (value === "") {
        value = value + "%20";
    }
    return value;
}
function convertParametroFecha(value) {
    var datetime1 = value.split('/');
    value = datetime1[0] + "%2F" + datetime1[1] + "%2F" + datetime1[2];
    return value;
}

function DeshabilitarControl(idInput, value) {
    if (idInput.indexOf("#") < 0)
        idInput = "#" + idInput;
    $(idInput).prop('disabled', value);
}


function messageSticky(msg) {
    $.sticky(msg, { autoclose: 2500, position: "top-right", type: "st-success" });
}

function messageStickyError(msg) {
    $.sticky(msg, { autoclose: 3500, position: "top-right", type: "st-error" });
}




/*--------------------Login--------------------*/

var Login = function() {
    
    IniciarSesion = function () {


        var jsonData = {
            "usuario_correo": $("#usuario_correo").val(),
            "usuario_password": $("#usuario_password").val()
        };


        $.post('IniciarSesion', jsonData,
         function (jsonResult) {
             if (jsonResult.success) {
                 if (!jsonResult.loginExitoso) {
                     $("#lblResLogin").html("Usuario o contraseña incorrecto.");
                 } else {
                     location.href = "/Entidad";
                 }

                 
             }
             else
                 MostrarAlerta(jsonResult.error);
         });
    }
    return {
        IniciarSesion: IniciarSesion
    }

}();

/*--------------------Entidad--------------------*/

var Entidad = function () {

    GetEntidad = function (idEntidad) {
        $("#msgAlerta").css("display", "none");
        var rowData = document.getElementById("row_" + idEntidad);
        $("#entidad_id").val(idEntidad);
        $("#entidad_nombre").val(rowData.cells[2].innerHTML);
        $("#entidad_dominio").val(rowData.cells[3].innerHTML);
        $("#entidad_cantidad_usuarios").val(rowData.cells[5].innerHTML);
        $("#EntityState").val("2");
        document.getElementById("stActivo").checked =  rowData.cells[6].innerHTML === "Activo" ? true:false;
        $("#btnhdNuevo").click();

    }
    GetDeleteEntidad = function (idEntidad) {
        debugger;
        var rowData = document.getElementById("row_" + idEntidad);
        if (rowData.cells[4].innerHTML > 0) {
            messageStickyError("No se puede eliminar el registro porque tiene usuarios asociados.");
            return false;
        }
        $("#entidad_id").val(idEntidad);
        $("#lblEntidad").html(rowData.cells[2].innerHTML);
        $('#confirm-delete').modal('show');
    }


    DeleteEntidad = function () {
        
        var entidad_id = $("#entidad_id").val();

        var jsonData = {
            "entidad_id": entidad_id
        };


        $.post('Entidad/DeleteEntidad', jsonData,
         function (jsonResult) {
             if (jsonResult.success) {
                 $("#btnBuscar").click();
                 $("#btnCancelar").click();
                 messageSticky("Se elimino correctamente el registro. \n");
             }
             else
                 MostrarAlerta(jsonResult.error);
         });
    }

    UpdateEntidad = function (jsonData) {
        $.post('Entidad/UpdateEntidad', jsonData,
            function (jsonResult) {
                if (jsonResult.success) {
                    $("#btnBuscar").click();
                    $("#btnCancelar").click();
                    messageSticky("Se actualizarón los datos correctamente. \n");
                }
                else
                    MostrarAlerta(jsonResult.error);
            });
    }

    SaveEntidad = function (jsonData) {
        $.post("Entidad/SaveEntidad", jsonData,
               function (jsonResult) {
                   if (jsonResult.success) {
                       $("#btnBuscar").click();
                       $("#btnCancelar").click();
                       messageSticky("Se registrarón los datos correctamente. \n");
                   }
                   else 
                       MostrarAlerta(jsonResult.error);
               });
    }
    return {
        GetDeleteEntidad:GetDeleteEntidad,
        GetEntidad: GetEntidad,
        SaveEntidad: SaveEntidad,
        UpdateEntidad: UpdateEntidad,
        DeleteEntidad: DeleteEntidad
    }

}();


/*--------------------Categoria--------------------*/

var Categoria = function () {

    GetCategoria = function (idCategoria) {
        $("#msgAlerta").css("display", "none");
        debugger;
        var rowData = document.getElementById("row_" + idCategoria);
        $("#categoria_objeto_id").val(idCategoria);
        $("#categoria_objeto_nombre").val(" " + rowData.cells[2].innerHTML);
        $("#imgCategoria").attr("src", "/Images/Categoria/"  + idCategoria  + ".png");
        $("#EntityState").val("2");
        $("#btnhdNuevo").click();

    }
    GetDeleteCategoria = function (idCategoria) {
        var rowData = document.getElementById("row_" + idCategoria);
       
        $("#categoria_objeto_id").val(idCategoria);
        $("#lblCategoria").html(rowData.cells[2].innerHTML);
        $('#confirm-delete').modal('show');
    }


    DeleteCategoria = function () {

        var Categoria_id = $("#categoria_objeto_id").val();

        var jsonData = {
            "categoria_objeto_id": categoria_objeto_id
        };


        $.post('Categoria/DeleteCategoria', jsonData,
         function (jsonResult) {
             if (jsonResult.success) {
                 $("#btnBuscar").click();
                 $("#btnCancelar").click();
                 messageSticky("Se elimino correctamente el registro. \n");
             }
             else
                 MostrarAlerta(jsonResult.error);
         });
    }

    UpdateCategoria = function (jsonData) {
        $.post('Categoria/UpdateCategoria', jsonData,
            function (jsonResult) {
                if (jsonResult.success) {
                    $("#btnBuscar").click();
                    $("#btnCancelar").click();
                    messageSticky("Se actualizarón los datos correctamente. \n");
                }
                else
                    MostrarAlerta(jsonResult.error);
            });
    }

    SaveCategoria = function (jsonData) {
        $.post("Categoria/SaveCategoria", jsonData,
               function (jsonResult) {
                   if (jsonResult.success) {
                       $("#btnBuscar").click();
                       $("#btnCancelar").click();
                       messageSticky("Se registrarón los datos correctamente. \n");
                   }
                   else
                       MostrarAlerta(jsonResult.error);
               });
    }
    return {
        GetDeleteCategoria: GetDeleteCategoria,
        GetCategoria: GetCategoria,
        SaveCategoria: SaveCategoria,
        UpdateCategoria: UpdateCategoria,
        DeleteCategoria: DeleteCategoria
    }

}();