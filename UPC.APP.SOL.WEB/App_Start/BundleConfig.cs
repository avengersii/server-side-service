﻿using System.Web;
using System.Web.Optimization;

namespace UPC.APP.SOL.WEB
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                          "~/Scripts/jquery/jquery-1.8.3.js",
                          "~/Scripts/jquery/jquery-ui-1.9.2.custom.js"
                          ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                         "~/Scripts/bootstrap/bootstrap.js",
                         "~/Scripts/bootstrap/bootstrap-datepicker.js"
                         ));
            bundles.Add(new ScriptBundle("~/bundles/Menu").Include(
                   "~/Scripts/Menu/menu.js",
                   "~/Scripts/Menu/jquery.hoverIntent.minified.js",
                   "~/Scripts/Menu/jquery.dcmegamenu.1.2.js"
                   ));

            bundles.Add(new ScriptBundle("~/bundles/gridGallery").Include(
                "~/Scripts/gridGallery/modernizr.custom.js",
                "~/Scripts/gridGallery/imagesloaded.pkgd.min.js",
                "~/Scripts/gridGallery/masonry.pkgd.min.js",
                "~/Scripts/gridGallery/classie.js",
                "~/Scripts/gridGallery/cbpGridGallery.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/general").Include(
                 "~/Scripts/general.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/treeView").Include(
                "~/Scripts/treeView/jquery.treetable.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/style/styles.css",
                "~/Content/style/menu.css",
                "~/Content/style/component.css"
                ));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                "~/Content/bootstrap/bootstrap.css",
                "~/Content/bootstrap/datepicker.css"
                ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                 "~/Content/themes/base/jquery-ui-1.9.2.custom.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css2").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}