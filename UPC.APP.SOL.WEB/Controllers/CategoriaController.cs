﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.APP.SOL.WEB.Core.utils;

namespace UPC.APP.SOL.WEB.Controllers
{
    public class CategoriaController : BaseController
    {
        BD_APPSOLIDARIDADEntities db = new BD_APPSOLIDARIDADEntities();
        public ActionResult Index()
        {
            if (!SessionActiva())
                return RedirectToAction("Login", "Home");


            return View();
        }

        public ActionResult SaveCategoria(t_categoria_objeto t_categoria_objeto)
        {
            try
            {
               
                    db.t_categoria_objeto.Add(t_categoria_objeto);
                    return Json(new { success = db.SaveChanges() != 0 });
              
                return Json(new { success = false, error = Utilitarios.GenerarMensajeError(ModelState) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        public ActionResult UpdateCategoria(t_categoria_objeto t_categoria_objeto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.t_categoria_objeto.Attach(t_categoria_objeto);
                    db.Entry(t_categoria_objeto).State = EntityState.Modified;
                    return Json(new { success = db.SaveChanges() != 0 });
                }
                return Json(new { success = false, error = Utilitarios.GenerarMensajeError(ModelState) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }
        public ActionResult DeleteCategoria(t_categoria_objeto categoria)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    t_categoria_objeto t_categoria_objeto = db.t_categoria_objeto.Find(categoria.categoria_objeto_id);
                    db.t_categoria_objeto.Remove(t_categoria_objeto);
                    return Json(new { success = db.SaveChanges() != 0 });
                }
                return Json(new { success = false, error = Utilitarios.GenerarMensajeError(ModelState) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }
        [HttpGet]
        public PartialViewResult GetCategoria(string value1)
        {

            string message = string.Empty;
            try
            {
                var res = from od in db.t_categoria_objeto
                          where
                   (od.categoria_objeto_nombre.Contains(value1))
                          select od;

                return PartialView("_GetCategoria", res);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return PartialView("_GetCategoria", null);

        }



    }
}
