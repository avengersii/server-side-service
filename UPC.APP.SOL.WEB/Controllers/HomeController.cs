﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.APP.SOL.WEB.Core.utils;

namespace UPC.APP.SOL.WEB.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            if (SessionActiva())
                return RedirectToAction("Index", "Entidad");

            return View();
        }
        BD_APPSOLIDARIDADEntities db = new BD_APPSOLIDARIDADEntities();
        public ActionResult IniciarSesion(t_usuario t_usuario)
        {
            try
            {
                var res = from od in db.t_usuario
                          where
                          (od.entidad_id == ConstantsSistema.ENTIDAD_ADMINISTRADOR) &&
                   (od.usuario_correo == t_usuario.usuario_correo) &&
                   (od.usuario_password == t_usuario.usuario_password)
                          select od;
                bool loginExitoso = false;
                if (res.ToList().Count() == 1)
                {
                    Session["Usuario"] = res.FirstOrDefault();
                    loginExitoso = true;
                }
                return Json(new { success = true, loginExitoso= loginExitoso });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        public ActionResult CerrarSesion()
        {

            Session["Usuario"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult About()
        {
           
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
