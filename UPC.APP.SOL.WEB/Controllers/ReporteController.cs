﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.APP.SOL.WEB.Core.utils;

namespace UPC.APP.SOL.WEB.Controllers
{
    public class ReporteController : BaseController
    {
        BD_APPSOLIDARIDADEntities db = new BD_APPSOLIDARIDADEntities();
        public ActionResult Index()
        {
            if (!SessionActiva())
                return RedirectToAction("Login", "Home");

            ViewBag.Estado = new SelectList(new[]
                                          {
                                              new {ID="POR",Name="Por Recuperar"},
                                              new {ID="REC",Name="Recuperado"},
                                              new{ID="ELI",Name="Eliminado"}
                                          },
                            "ID", "Name", 1);
            var resEntidades = from od in db.t_entidad
                where
                    (od.entidad_estado == "HAB") &&
                    (od.entidad_id != ConstantsSistema.ENTIDAD_ADMINISTRADOR)          
                               select od;

            ViewBag.Entidades = new SelectList(resEntidades.ToList(), "entidad_id", "entidad_nombre");

            ViewBag.Categoria = new SelectList(db.t_categoria_objeto.ToList(), "categoria_objeto_id", "categoria_objeto_nombre");
           
            return View();
        }

        [HttpGet]
        public PartialViewResult GetReporte(string value1, string value2,string value3)
        {

            string message = string.Empty;
            try
            {
                string sqlwhere = "";
                if (value1!=string.Empty)
                {
                    sqlwhere = " and entidad_id = " + value1;
                }
                if (value2 != string.Empty)
                {
                    sqlwhere += " and categoria_objeto_id = " + value2;
                }
                if (value3 != string.Empty)
                {
                    sqlwhere += " and reporte_estado = '" + value3 + "'";
                }
                var res = db.Database.SqlQuery<V_REPORTE>
                ("SELECT * FROM dbo.V_REPORTE WHERE 1=1 "  +  sqlwhere + " ORDER BY Fecha DESC").ToList();

                return PartialView("_GetReporte", res);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return PartialView("_GetReporte", null);

        }


    }
}
