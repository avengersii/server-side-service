﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.APP.SOL.WEB.Core.utils;

namespace UPC.APP.SOL.WEB.Controllers
{
    public class EntidadController : BaseController
    {
        BD_APPSOLIDARIDADEntities db = new BD_APPSOLIDARIDADEntities();

        public ActionResult Index()
        {
            if (!SessionActiva())
                return RedirectToAction("Login", "Home");

            ViewBag.Estado = new SelectList(new[]
                                          {
                                              new {ID="HAB",Name="Activo"},
                                              new{ID="DES",Name="Inactivo"}
                                          },
                            "ID", "Name", 1);

            return View();
        }
       
        public ActionResult SaveEntidad(t_entidad t_entidad)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.t_entidad.Add(t_entidad);
                    return Json(new { success = db.SaveChanges() != 0 });
                }
                return Json(new { success = false, error = Utilitarios.GenerarMensajeError(ModelState) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        public ActionResult UpdateEntidad(t_entidad t_entidad)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.t_entidad.Attach(t_entidad);
                    db.Entry(t_entidad).State = EntityState.Modified;
                    return Json(new { success = db.SaveChanges() != 0 });
                }
                return Json(new { success = false, error = Utilitarios.GenerarMensajeError(ModelState) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }
        public ActionResult DeleteEntidad(t_entidad t_entidad)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    t_entidad.entidad_estado = "ELI";
                    db.Entry(t_entidad).State = EntityState.Modified;
                    return Json(new { success = db.SaveChanges() != 0 });
                }
                return Json(new { success = false, error = Utilitarios.GenerarMensajeError(ModelState) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }
        [HttpGet]
        public PartialViewResult GetEntidades(string value1, string value2)
        {

            string message = string.Empty;
            try
            {
                var res = from od in db.t_entidad
                          where
                          (od.entidad_estado == (value2 != "" ? value2 : od.entidad_estado)) &&
                   (od.entidad_nombre.Contains(value1)) &&
                   (od.entidad_estado!="ELI") &&
                   (od.entidad_id != ConstantsSistema.ENTIDAD_ADMINISTRADOR) 
                          select od;

                return PartialView("_GetEntidades", res);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return PartialView("_GetEntidades", null);

        }

    }
}
