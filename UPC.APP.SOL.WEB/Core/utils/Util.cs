﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;

namespace UPC.APP.SOL.WEB.Core.utils
{
    public static class ConstantsImages
    {
        public static readonly string NUEVO = "nuevo";
        public static readonly string GUARDAR = "guardar";
        public static readonly string EDITAR = "editar";
        public static readonly string ELIMINAR = "eliminar";
        public static readonly string VER = "ver";

    }
    public static class ConstantsSistema
    {
        public static readonly int  ENTIDAD_ADMINISTRADOR = 1;

    }
     public  class Utilitarios
    {
       public static string GenerarMensajeError(ModelStateDictionary ModelState)
        {
            StringBuilder strError = new StringBuilder();
            strError.AppendLine("<ul>");
            foreach (var error in ModelState)
            {
                if (error.Value.Errors.Count() != 0)
                {
                    strError.AppendLine("<li>" + string.Format("{0}", error.Value.Errors[0].ErrorMessage) + "</li>");

                }
            }
            strError.AppendLine("</ul>");
            return strError.ToString(); ;

        }

     
    }

    
}