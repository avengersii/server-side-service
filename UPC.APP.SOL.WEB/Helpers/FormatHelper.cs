﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace UPC.APP.SOL.WEB.Helpers
{
  public static class FormatHelper
    {
        public static MvcHtmlString CustomTd(this HtmlHelper helper, string value,  string style = null)
        {

            TagBuilder td = new TagBuilder("td");
           
            td.Attributes["style"] = style;
            td.InnerHtml = value;

            return new MvcHtmlString(td.ToString());
        }
        public static MvcHtmlString CustomTdCheckBox(this HtmlHelper helper, string value, bool center=false ,string style = null)
        {

            TagBuilder td = new TagBuilder("td");

            if (center)
                style += "text-align:center";

            td.Attributes["style"] = style;

            TagBuilder checkbox = new TagBuilder("input");
            checkbox.MergeAttribute("type", "checkbox");
            if (value=="True")
                checkbox.MergeAttribute("checked","");

            td.InnerHtml = checkbox.ToString();

            return new MvcHtmlString(td.ToString());
        }


        public static MvcHtmlString CustomTdImage(this HtmlHelper helper, string imagen, string onclick)
        {
            TagBuilder td = new TagBuilder("td");

            td.Attributes["style"] = "text-align:center";
            TagBuilder img  = new TagBuilder("img");
            img.Attributes["height"] = "18";
            img.Attributes["width"] = "18";
            img.Attributes["style"] = "cursor: pointer;";
            img.Attributes["src"] = "/Images/" + imagen + ".png";
            if (onclick != "")
                img.Attributes["onclick"] = onclick;

            td.InnerHtml = img.ToString();

            return new MvcHtmlString(td.ToString());
        }

    }
}